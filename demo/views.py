from django.shortcuts import render

# Create your views here.
def v_index(request):
	template = 'index.html'
	return render(request, template, {})